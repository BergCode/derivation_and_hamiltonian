\contentsline {section}{\tocsection {}{1}{Introduction}}{1}{}%
\contentsline {section}{\tocsection {}{2}{Schrödinger \& Manakov equation derivations}}{1}{}%
\contentsline {subsection}{\tocsubsection {}{2.1}{Deterministic Manakov equation}}{1}{}%
\contentsline {subsection}{\tocsubsection {}{2.2}{Stochastic Schrödinger equation}}{2}{}%
\contentsline {subsection}{\tocsubsection {}{2.3}{Stochastic Manakov equation}}{2}{}%
\contentsline {section}{\tocsection {}{3}{Hamiltonian of Schrödinger equations}}{2}{}%
\contentsline {section}{\tocsection {}{}{References}}{2}{}%
